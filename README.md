# Prepare Virtual Box Settings (Linux Ubuntu 16.04 LTS)

1. Download Ubuntu Iso [http://www.ubuntu.com/download/server/thank-you?version=16.04.1&architecture=amd64](http://www.ubuntu.com/download/server/thank-you?version=16.04.1&architecture=amd64).
1. Specify name for virtual os (vagrant-ubuntu-box).
1. Specify type (Linux).
1. Specify version (Ubuntu 64-bit).
1. Create VMDK virtual disk with dynamic allocated disk size of 40G.
1. Select System tab and have a base memory of 512 MB select only optical and hard disk for boot order and enable extended feature Enable I/O APIC.
1. Still on the System tab select the Processer sub tab and have Processor(s) to 2 and Execution Cap to 100%.
1. Select the Display tab and have Video Memory set to 27MB, Monitor Count to 1 and leave the other settings and sub tabs to defaults.
1. Leave the Storage tab at default.
1. Select the Audio tab and uncheck Enable Audio.
1. Select the Network Tab, Adapter 1 sub tab enable Network Adapter and have the Attached ot NAT.
1. Still on Adapter 1 tab, select Advanced drop down and click on port forwarding click on the plus icon.
1. After clicking plus icon provide Name vagrant-ssh, Protocol TCP, Host IP 127.0.0.1, Host Port 2222, Guest IP leave blank, Guest Port 22.
1. Leave Serial Ports at default.
1. Uncheck Enable USB Controller for USB TAB.
1. Leave Shared Folders at its default.
1. Leave User Interface at its default.
1. Start the virtual box and point to the downloaded Ubuntu iso.

# Setting Up Ubuntu Installation

Go through the installation process make sure to select install open ssh server when prompted (only my case for base vagrant box), however once prompted for username and password provide the below username and password.
1. User to create vagrant
1. User's password vagrant
It is very important to create vagrant username and vagrant password otherwise vagrant won't be able to connect to the box.

Once installation is complete log into your virtual machine instance and do the bewlow setup:

```bash
# I prefer using vim over nano.
$ sudo update-alternatives --config editor
# type the number leading vim.

# We need to change some settings to sudo
$ sudo visudo
# Add the below two lines under the Default section.
Defaults !requiretty
Defaults env_keep+="SSH_AUTH_SOCK"
# Alter the below line to
# Allow members of group sudo to execute any command
# Before
%sudo   ALL=(ALL:ALL) ALL
# After (this allow to invoke sudo without prompt for password important for vagrant).
%sudo   ALL=(ALL:ALL) NOPASSWD: ALL

# Configure sshd and add the below lines
$ sudo vim /etc/ssh/sshd_config
Port 22
AuthorizedKeysFile %h/.ssh
UseDNS no 

# Restart ssh, note Ubuntu 16.04 make use of systemd
sudo systemctl restart ssh

# Minimize the wait time for grub to 1 sec, makes loading vagrant box faster relatively.
$ sudo vim /etc/default/grub
# Change GRUB_TIMEOUT = 2 to
GRUB_TIMEOUT = 1
$ sudo grub-mkconfig -o /boot/grub/grub.cfg

# Create ssh directory
$ mkdir .ssh && cd .ssh
$ wget --no-check-certificate https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub -O authorized_keys
$ cd .. 
$ chmod 0700 .ssh
$ chmod 0600 .ssh/authorized_keys

# Prepare image for installing Virtual box guest additions (Important)
$ sudo apt-get install -y dkms build-essentials linux-headers-generic linux-headers-$(uname -r)
$ sudo shutdown -r now
$ mkdir /mnt/cdrom
$ mount /dev/cdrom /mnt/cdrom
# From the device menu of virtual box select insert guest additions cd.
$ cd /mnt/cdrom
[/mnt/cdrom] $ ./VBoxLinuxAddition.run
$ cd ~
$ umout /mnt/cdrom
$ sudo shutdown -r now
# verify vboxsf  installed by running grep on lsmod.
$ lsmod | grep vbox

# Add 1M of zeros to disk block.
sudo dd if=/dev/zero of=/DELETE bs=1M
sudo rm -rf /DELETE

# Turn off virtual box.
$ sudo shutdown -h now

```
Now we need to package the virtual image into vagrant consumbale box.
On the host machine given that vagrant is already installed and is on latest.

```bash
[host-machine] $ mkdir -p ~/Boxes/bases && cd ~/Boxes/bases
[host-machine] $ vagrant package --base vagrant-ubuntu-box -o ubuntu-server.box
[host-machine] $ vagrant box add ubuntu-server ubuntu-server.box
[host-machine] $ mkdir -p ~/Boxes/test && cd ~/Boxes/test
[host-machine] $ vagrant init ubuntu-server
[host-machine] $ vagrant up
[host-machine] $ vagrant ssh
# If all is fine you should be logged into vagrant ubuntu base box.
```
